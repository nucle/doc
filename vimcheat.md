# Vim sheat

## Se déplacer H

### Par colonne
> **h** 🠔  ⌨  🠖  **l**

### Par mot
> **B** 🠔  **b** ⌨ **e** 🠖  **w**

### Ligne
> **0** 🠔  **^** ⌨ 🠖  **$**

## Se déplacer V

### Par ligne
> &nbsp;**k**  
> &nbsp;🠕  
> ⌨  
> &nbsp;🠗  
> &nbsp;**j**  

### Par n° de ligne
> *n***G**           *(ex: 18G)*

### Par nombre de ligne
> &nbsp;*n***k**  
> &nbsp;🠕  
> ⌨  
> &nbsp;🠗  
> &nbsp;*n***j**  

### Début/Fin de fichier
> &nbsp;**gg**  
> &nbsp;🠕  
> ⌨  
> &nbsp;🠗  
> &nbsp;**G**  

### Jusqu'a un caractère x
> d**t**x : Effacer jusqu'à x  
> y**t**" : Copier tout jusqu'à "  
> "+y**t**C : Copier dans le presse papier général jusqu'à C  

## Remplacer

### Une ligne
> **:s/**_search_**/**_replace_**/g**

### Tout le fichier
> **:%s/**_search_**/**_replace_**/g**

### Avec confirmation
> **:%s/**_search_**/**_replace_**/gc**

### Insensible à la casse
> **:%s/**_search_**/**_replace_**/gi**

### Utiliser les registres
> **:%s/**_search_**/\=@**_register_**/g**  
 
### Caractères particuliers
> <u>RECHERCHE</u>  
> **\\** : Caractère d'échappement  
> **\s** : Chercher un espace  
> **\t** : Chercher une tabulation  
> **[]** : Tous les termes entre crochets 
>> *(ex supprimer les voyelles: s/[aeiouy]//gi )*   
    
> <u>REMPLACEMENT</u>  
> **\\r** : nouvelle ligne  
> **\\n** : caractère null  

### Supprimer le highlight
> **:noh**

### annuler/refaire
> **u** : Annuler  
> **ctrl-r**: Refaire


### Mettre en MAJUSCULE/minuscule
> **gU**w : Met en majuscule tout un mot  
> **gu**$ : Met en minuscule le reste de la ligne  

### Indenter le code de toute la page
> **gg=G**

## Registre

### Copier/coller dans le registre system
> **"+y** : Copier  
> **"+p** : Coller  

### Afficher les registres
> **:reg** : Afficher les registres

## Macro
> **q**a : Lance l'enregistrement de la macro a  
> **q** : Stoppe l'enregistrement de la macro  
> <b>@</b>a : Exécute la macro a  

## Marques
> **:marks** : Afficher les marques  
> **m**a : Ajouter une marque a  
> <b>'</b>a : Se rendre à une marque a  

## Fenêtre/Buffer/tab
> **:new**/**:vnew** : Nouvelle fenêtre avec nouveau buffer (horizontale/verticale).  
> **sp**/**vsp** : Nouvelle fenêtre  
> **:tabe** : Nouveau tab   
> **:tabf** : nouveau tab avec fichier  

> **:tabs** : Liste des tabs  
> **:ls** : Liste des buffers  

> **ctrl-w w**: Changer de fenêtre  
> **gt**/**gT**/3**gt**: Changer de tab (suivant/précédent/tab 3)  

> **:q** : Fermer une fenêtre  
> **:tab close**/**:q** : Fermer une tab  
> **:bd** : Fermer un buffer  

> **:tab ball** : transformer tout en 1 Buffer = 1 fenetre et 1 tab  


